using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using SimpleLambda;

namespace SimpleLambda.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            
            Environment.SetEnvironmentVariable("championProductsDbConnectionString", "Server=testinstance-dev.csftyngh5sqr.us-east-2.rds.amazonaws.com;UID=mbergmann;PWD=MyP@ssw0rd");

            var success = function.FunctionHandler(context);

            Assert.True(success);
        }
    }
}
